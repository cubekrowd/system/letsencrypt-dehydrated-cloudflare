FROM ubuntu:18.04
MAINTAINER Foorack <maxfaxalv@gmail.com>

RUN apk add --no-cache git curl sed grep gawk coreutils diffutils python3 python3-pip && \ 
    git clone https://github.com/lukas2511/dehydrated.git /dehydrated && \
    mkdir /dehydrated/hooks && \
    git clone https://github.com/kappataumu/letsencrypt-cloudflare-hook /dehydrated/hooks/cloudflare

WORKDIR /dehydrated

RUN pip install -r hooks/cloudflare/requirements.txt && \
    ln -s /usr/bin/python3 /usr/bin/python && \
    /dehydrated/dehydrated --register --accept-terms

ENTRYPOINT ["./dehydrated"]
CMD ['-h']
#ENTRYPOINT ["/bin/bash"]